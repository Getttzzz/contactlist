package contactlist.interfaces;

import contactlist.model.Contact;

public interface IContactList {
    void createContact(Contact contact);
    void deleteContact(Contact contact);
}
