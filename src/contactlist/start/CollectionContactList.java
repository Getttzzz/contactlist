package contactlist.start;

import contactlist.interfaces.IContactList;
import contactlist.model.Contact;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;

public class CollectionContactList implements IContactList {

    private ObservableList<Contact> mContactsList = FXCollections.observableArrayList();

    /**
     * Create new contact
     * @param contact
     */
    @Override
    public void createContact(Contact contact) {
        mContactsList.add(contact);
    }

    /**
     * Delete contact
     * @param contact
     */
    @Override
    public void deleteContact(Contact contact) {
        mContactsList.remove(contact);
    }

    /**
     * Getter
     * @return
     */
    public ObservableList<Contact> getContactsList() {
        return mContactsList;
    }
}
