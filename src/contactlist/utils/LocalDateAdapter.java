package contactlist.utils;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;

/**
 * Created by Getz on 30.11.2015.
 */
public class LocalDateAdapter extends XmlAdapter<String, LocalDate>{

    /**
     * Method for unmarshalling strings
     * @param v
     * @return
     * @throws Exception
     */
    @Override
    public LocalDate unmarshal(String v) throws Exception {
        return LocalDate.parse(v);
    }

    /**
     * Method for marshaling strings
     * @param v
     * @return
     * @throws Exception
     */
    @Override
    public String marshal(LocalDate v) throws Exception {
        return v.toString();
    }
}
