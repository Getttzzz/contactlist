package contactlist.model;

import contactlist.utils.LocalDateAdapter;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;

public class Contact {

    private SimpleStringProperty mName = new SimpleStringProperty("");
    private SimpleStringProperty mSurname = new SimpleStringProperty("");
    private SimpleStringProperty mPhone = new SimpleStringProperty("");
    private SimpleStringProperty mAddress = new SimpleStringProperty("");
    private SimpleStringProperty mEmail= new SimpleStringProperty("");
    private ObjectProperty<LocalDate> mBirthday = new SimpleObjectProperty<LocalDate>(LocalDate.of(1995, 1, 1));
    private SimpleStringProperty mImagePath = new SimpleStringProperty("");

    /**
     * Default Constructor
     */
    public Contact(){
    }

    /**
     * Custom Constructor
     * @param pName
     * @param pSurname
     */
    public Contact(String pName, String pSurname) {
        this.mName = new SimpleStringProperty(pName);
        this.mSurname = new SimpleStringProperty(pSurname);
        this.mPhone = new SimpleStringProperty("725-55-55");
        this.mAddress = new SimpleStringProperty("Brooklyn, Nested.av, 15");
        this.mEmail = new SimpleStringProperty("person@gmail.com");
        this.mBirthday = new SimpleObjectProperty<LocalDate>(LocalDate.of(1999, 2, 21));
    }

    public String getName() {
        return mName.get();
    }

    public StringProperty NameProperty() {
        return mName;
    }

    public void setName(String mName) {
        this.mName.set(mName);
    }

    public String getSurname() {
        return mSurname.get();
    }

    public StringProperty SurnameProperty() {
        return mSurname;
    }

    public void setSurname(String mSurname) {
        this.mSurname.set(mSurname);
    }

    public String getPhone() {
        return mPhone.get();
    }

    public StringProperty PhoneProperty() {
        return mPhone;
    }

    public void setPhone(String mPhone) {
        this.mPhone.set(mPhone);
    }

    public String getAddress() {
        return mAddress.get();
    }

    public StringProperty AddressProperty() {
        return mAddress;
    }

    public void setAddress(String mAddress) {
        this.mAddress.set(mAddress);
    }

    public String getEmail() {
        return mEmail.get();
    }

    public StringProperty EmailProperty() {
        return mEmail;
    }

    public void setEmail(String mEmail) {
        this.mEmail.set(mEmail);
    }

    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    public LocalDate getBirthday() {
        return mBirthday.get();
    }

    public ObjectProperty<LocalDate> BirthdayProperty() {
        return mBirthday;
    }

    public void setBirthday(LocalDate mBirthday) {
        this.mBirthday.set(mBirthday);
    }

    public String getImagePath() {

        return mImagePath.get();
    }

    public SimpleStringProperty imagePathProperty() {
        return mImagePath;
    }

    public void setImagePath(String pImagePath) {
        this.mImagePath.set(pImagePath);
    }
}
