package contactlist.utils;

import javafx.scene.control.*;
import javafx.stage.StageStyle;

public class FxDialogs {

    /**
     * Static method for showing information dialog
     * @param title
     * @param message
     */
    public static void showInformation(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.initStyle(StageStyle.UTILITY);
        alert.setTitle("Information");
        alert.setHeaderText(title);
        alert.setContentText(message);
        alert.showAndWait();
    }
}