package contactlist.utils;

import contactlist.start.CollectionContactList;
import contactlist.start.ContactListWrapper;
import contactlist.start.Main;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.util.prefs.Preferences;

public class SaveUtils {

    private CollectionContactList mCollectionContactList;
    private Stage mPrimaryStage;

    /**
     *Custom Constructor
     * @param pCollectionContactList
     * @param pPrimaryStage
     */
    public SaveUtils(CollectionContactList pCollectionContactList, Stage pPrimaryStage) {
        this.mCollectionContactList = pCollectionContactList;
        this.mPrimaryStage = pPrimaryStage;
    }

    /**
     *Create new file path
     */
    public void handleNew() {
        mCollectionContactList.getContactsList().clear();
        setContactFilePath(null);
    }

    /**
     * Open dialog for open select existing file
     */
   public void handleOpen() {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showOpenDialog(mPrimaryStage);
        if (file != null) {
            loadContactDataFromFile(file);
        }
    }

    /**
     * Save current file
     */
   public void handleSave() {
        File personFile = getContactFilePath();
        if (personFile != null) {
            saveContactDataToFile(personFile);
        } else {
            handleSaveAs();
        }
    }

    /**
     * Open save dialog
     */
   public void handleSaveAs() {
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showSaveDialog(mPrimaryStage);
        if (file != null) {
            if (!file.getPath().endsWith(".xml")) {
                file = new File(file.getPath() + ".xml");
            }
            saveContactDataToFile(file);
        }
    }

    /**
     * Open info dialog
     */
    public void handleAbout() {
        FxDialogs.showInformation("Contact list application", "Creator: Yuriy Getsko, 525st");
    }

    /**
     * Exit from program
     */
    public void handleExit() {
        System.exit(0);
    }

    /**
     * Load contact data from existing file
     * @param file
     */
    public void loadContactDataFromFile(File file) {
        try {
            JAXBContext context = JAXBContext.newInstance(ContactListWrapper.class);
            Unmarshaller um = context.createUnmarshaller();
            // Reading XML from the file and unmarshalling.
            ContactListWrapper wrapper = (ContactListWrapper) um.unmarshal(file);

            mCollectionContactList.getContactsList().clear();
            mCollectionContactList.getContactsList().addAll(wrapper.getContacts());
            // Save the file path to the registry.
            setContactFilePath(file);

        } catch (Exception e) { // catches ANY exception
            FxDialogs.showInformation("Error", "Could not load data from file:\n" + file.getPath());
        }
    }

    /**
     * Save contact data to file
     * @param file
     */
    public void saveContactDataToFile(File file) {
        try {
            JAXBContext context = JAXBContext.newInstance(ContactListWrapper.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            // Wrapping our person data.
            ContactListWrapper wrapper = new ContactListWrapper();
            wrapper.setContacts(mCollectionContactList.getContactsList());

            // Marshalling and saving XML to the file.
            m.marshal(wrapper, file);

            // Save the file path to the registry.
            setContactFilePath(file);
        } catch (Exception e) { // catches ANY exception
            FxDialogs.showInformation("Error", "Could not save data to file:\n" + file.getPath());
        }
    }

    /**
     * Get last contact file path
     * @return
     */
    public File getContactFilePath() {
        Preferences prefs = Preferences.userNodeForPackage(Main.class);
        String filePath = prefs.get("filePath", null);
        if (filePath != null) {
            return new File(filePath);
        } else {
            return null;
        }
    }

    /**
     * Set last contact file path
     * @param file
     */
    public void setContactFilePath(File file) {
        Preferences prefs = Preferences.userNodeForPackage(Main.class);
        if (file != null) {
            prefs.put("filePath", file.getPath());
            mPrimaryStage.setTitle("Contact list - " + file.getName());
        } else {
            prefs.remove("filePath");
            mPrimaryStage.setTitle("Contact list");
        }
    }

    /**
     * Get last contact list
     */
    public void getLastContactList(){
        File file = getContactFilePath();
        if (file != null) {
            loadContactDataFromFile(file);
        }
    }

    /**
     * Open diaog for select image
     * @return
     */
    public File handleOpenImage() {
        File file = null;
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter1 = new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.jpg");
        fileChooser.getExtensionFilters().add(extFilter1);
        file = fileChooser.showOpenDialog(mPrimaryStage);
        return file;
    }

}
