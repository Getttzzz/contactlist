package contactlist.controllers;

import contactlist.model.Contact;
import contactlist.start.CollectionContactList;
import contactlist.start.Main;
import contactlist.utils.FxDialogs;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;


public class EditDialogController {

    @FXML
    private TextField txtName;
    @FXML
    private TextField txtSurname;
    @FXML
    private TextField txtPhone;
    @FXML
    private TextField txtAddress;
    @FXML
    private TextField txtEmail;
    @FXML
    private DatePicker datePickerBirthday;
    @FXML
    private ImageView ivPhoto;
    @FXML
    private TextArea lbPath;

    private Contact mContact;
    private Main mMain;
    private Image mImage;
    private CollectionContactList mCollectionContactList;
    private boolean mAction;
    private File mFile;

    /**
     * Setter
     * @param pCollectionContactList
     */
    public void setCollectionContactList(CollectionContactList pCollectionContactList) {
        mCollectionContactList = pCollectionContactList;

    }

    /**
     * Setter
     * @param pAction
     */
    public void setAction(boolean pAction) {
        mAction = pAction;
    }

    /**
     * Setter
     * @param mMain
     */
    public void setMain(Main mMain) {
        this.mMain = mMain;
    }

    /**
     * Set contacts in text fields
     * @param pContact
     */
    public void setContactForEdit(Contact pContact) {
        if (pContact == null) {
            return;
        }
        mContact = pContact;
        txtName.setText(pContact.getName());
        txtSurname.setText(pContact.getSurname());
        txtPhone.setText(pContact.getPhone());
        txtAddress.setText(pContact.getAddress());
        txtEmail.setText(pContact.getEmail());
        datePickerBirthday.setValue(pContact.getBirthday());
        lbPath.setText(pContact.getImagePath());

        File file = new File(pContact.getImagePath());

        if(!file.exists()){
            mImage = new Image(getClass().getResource("/contactlist/drawable/ic_portrait_black_48dp.png").toString());
            lbPath.setText("");
        } else {
            mImage = new Image(file.toURI().toString());
        }
        ivPhoto.setImage(mImage);
    }

    /**
     * Cleat all fields
     */
    public void clearAllFields() {
        txtName.setText("");
        txtSurname.setText("");
        txtPhone.setText("");
        txtAddress.setText("");
        txtEmail.setText("");
        datePickerBirthday.setValue(LocalDate.of(1990, 1, 1));
        mImage = new Image(getClass().getResource("/contactlist/drawable/ic_portrait_black_48dp.png").toString());
        lbPath.setText("");
    }

    /**
     * Close modal form
     * @param actionEvent
     */
    public void actionClose(ActionEvent actionEvent) {
        Node sourse = (Node) actionEvent.getSource();
        Stage stage = (Stage) sourse.getScene().getWindow();
        Image image = new Image(getClass().getResource("/contactlist/drawable/ic_portrait_black_48dp.png").toString());
        ivPhoto.setImage(image);
        lbPath.setText("empty");
        stage.hide();
    }

    /**
     * Call whan user click to add btn
     * @param actionEvent
     */
    public void actionAdd(ActionEvent actionEvent) {
        setActionCreate(mAction, actionEvent);

    }

    /**
     * Define what action user call
     * @param pAction
     * @param pActionEvent
     */
    public void setActionCreate(boolean pAction, ActionEvent pActionEvent){
        if(pAction){
            if(isValid()){
                mContact = new Contact();
                validationAndSaveToModel(pActionEvent);
                mCollectionContactList.createContact(mContact);
                actionClose(pActionEvent);
            }
        } else {
            validationAndSaveToModel(pActionEvent);
        }
    }

    /**
     * Validation and save in model
     * @param pActionEvent
     */
    private void validationAndSaveToModel(ActionEvent pActionEvent) {
        if (isValid()) {
            mContact.setName(txtName.getText().trim().replaceAll("\\s+", ""));
            mContact.setSurname(txtSurname.getText().trim().replaceAll("\\s+", ""));
            mContact.setPhone(txtPhone.getText().trim().replaceAll("\\s+", ""));
            mContact.setAddress(txtAddress.getText().trim().replaceAll("\\s+", ""));
            mContact.setEmail(txtEmail.getText().trim().replaceAll("\\s+", ""));
            mContact.setBirthday(datePickerBirthday.getValue());
            mContact.setImagePath(lbPath.getText());
            actionClose(pActionEvent);
        }
    }

    /**
     * Check all fields on empty strings
     * @return
     */
    private boolean isValid() {
        String errorMessage = "";

        if (txtName.getText() == null || txtName.getText().trim().length() == 0) {
            errorMessage += "No valid First name!\n";
        }

        if (txtSurname.getText() == null || txtSurname.getText().trim().length() == 0) {
            errorMessage += "No valid Surname name!\n";
        }

        if (txtPhone.getText() == null || txtPhone.getText().trim().length() == 0) {
            errorMessage += "No valid Phone!\n";
        }

        if (txtAddress.getText() == null || txtAddress.getText().trim().length() == 0) {
            errorMessage += "No valid Address!\n";
        }

        if (txtEmail.getText() == null || txtEmail.getText().trim().length() == 0) {
            errorMessage += "No valid Email!\n";
        }

        if (datePickerBirthday.getValue() == null) {
            errorMessage += "No valid Date!\n";
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            FxDialogs.showInformation("Invalid Fields", errorMessage);
            return false;
        }
    }

    /**
     * Call dialog select and save image in other place
     * @param pActionEvent
     */
    public void actionSelectImage(ActionEvent pActionEvent) {
        if (txtName.getText().trim().isEmpty() || txtSurname.getText().trim().isEmpty() || txtPhone.getText().trim().isEmpty() || txtAddress.getText().trim().isEmpty()) {
            FxDialogs.showInformation("Empty fields", "Please, fill name, surname, phone and address.");
            return;
        }
        mFile = mMain.getSaveUtils().handleOpenImage();
        try {
            String path = System.getProperty("user.home") + File.separator + "Documents";
            path += File.separator + "user_avatars";
            File customDir = new File(path);
            customDir.mkdirs();
            File fileImage = new File(path + File.separator + txtName.getText() + txtSurname.getText() +txtPhone.getText()+txtAddress.getText()+ ".jpg");
            mImage = new Image(mFile.toURI().toString());
            writeImageToFile(mImage, fileImage);
            mImage = new Image(fileImage.toURI().toString());
            ivPhoto.setImage(mImage);
            lbPath.setText(fileImage.getAbsolutePath());
        } catch (Exception e) {
            e.printStackTrace();
            FxDialogs.showInformation("Warning!", "File not created.");
        }
    }

    /**
     * Write image in certain place
     * @param pImage
     * @param pFileImage
     */
    private void writeImageToFile(Image pImage, File pFileImage) {
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(pImage, null), "jpg", pFileImage);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
