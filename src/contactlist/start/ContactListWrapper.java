package contactlist.start;

import contactlist.model.Contact;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "contacts")
public class ContactListWrapper {
    private List<Contact> mContacts;

    /**
     * Getter
     * @return
     */
    @XmlElement(name = "contact")
    public List<Contact> getContacts(){
        return mContacts;
    }

    /**
     * Setter
     * @param pContacts
     */
    public void setContacts(List<Contact> pContacts){
        this.mContacts = pContacts;
    }
}
