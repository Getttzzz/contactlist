package contactlist.controllers;

import contactlist.model.Contact;
import contactlist.start.CollectionContactList;
import contactlist.start.Main;
import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

public class MainController {

    @FXML
    private Button btCreateContact;
    @FXML
    private Button btEditContact;
    @FXML
    private Button btDeleteContact;
    @FXML
    private TableView contactMainTable;
    @FXML
    private TableColumn<Contact, String> nameColumn;
    @FXML
    private TableColumn<Contact, String> surnameColumn;
    @FXML
    private TableColumn<Contact, String> phoneColumn;
    @FXML
    private TableColumn<Contact, String> addressColumn;
    @FXML
    private TableColumn<Contact, String> emailColumn;
    @FXML
    private TableColumn<Contact, String> birthdayColumn;
    @FXML
    private Label labelCount;
    @FXML
    private Button btnSearch;
    @FXML
    private TextField txtSearch;


    private Main mMain;
    private CollectionContactList mCollectionContactList = new CollectionContactList();
    private Stage mainStage;
    private Parent mParentEdit;
    private FXMLLoader fxmlLoader = new FXMLLoader();
    private EditDialogController mEditDialogController;
    private Stage mEditDialogStage;
    private ArrayList<Contact> mTempContactList;
    private boolean firstClick = true;
    private int mCountOfContacts;
    private int mOldValueCountOfContacts;

    /**
     * Setter
     * @param mainStage
     */
    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }

    /**
     * Setter
     * @param mMain
     */
    public void setMain(Main mMain) {
        this.mMain = mMain;
    }

    /**
     * Getter
     * @return
     */
    public CollectionContactList getCollectionContactList() {
        return mCollectionContactList;
    }

    /**
     * Setter
     * @param pCollectionContactList
     */
    public void setCollectionContactList(CollectionContactList pCollectionContactList) {
        mCollectionContactList = pCollectionContactList;
    }

    /**
     *Call when scene created (Entry point)
     */
    @FXML
    private void initialize() {
        nameColumn.setCellValueFactory(new PropertyValueFactory<Contact, String>("Name"));
        surnameColumn.setCellValueFactory(new PropertyValueFactory<Contact, String>("Surname"));
        phoneColumn.setCellValueFactory(new PropertyValueFactory<Contact, String>("Phone"));
        addressColumn.setCellValueFactory(new PropertyValueFactory<Contact, String>("Address"));
        emailColumn.setCellValueFactory(new PropertyValueFactory<Contact, String>("Email"));
        birthdayColumn.setCellValueFactory(new PropertyValueFactory<Contact, String>("Birthday"));

        initListeners();
        initLoader();
        initData();
    }

    /**
     * Initialization data from contactlist collection
     */
    private void initData() {
        contactMainTable.setItems(mCollectionContactList.getContactsList());
    }

    /**
     * Initialization all listeners
     */
    private void initListeners() {
        mCollectionContactList.getContactsList().addListener(new ListChangeListener<Contact>() {
            @Override
            public void onChanged(Change<? extends Contact> c) {
                updateCountLabel();
            }
        });
        contactMainTable.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount() == 2) {
                    mEditDialogController.setAction(false);
                    mEditDialogController.setContactForEdit((Contact) contactMainTable.getSelectionModel().getSelectedItem());
                    showEditDialog();
                }
            }
        });
    }

    /**
     * Initialization loader for scene
     */
    private void initLoader() {
        try {
            fxmlLoader.setLocation(getClass().getResource("../xml/create_user_stage.fxml"));
            mParentEdit = fxmlLoader.load();
            mEditDialogController = fxmlLoader.getController();
//            mEditDialogController.setMain(mMain);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Updating count of contacts
     */
    private void updateCountLabel() {
        mCountOfContacts = mCollectionContactList.getContactsList().size();
        labelCount.setText("Count of notes: " + mCountOfContacts);
    }

    /**
     * Call when user click on create or edit or delete or search
     * @param actionEvent
     */
    public void actionButtonPressed(ActionEvent actionEvent) {
        Object sourse = actionEvent.getSource();
        if (!(sourse instanceof Button)) {
            return;
        }
        Button clickedButton = (Button) sourse;
        switch (clickedButton.getId()) {
            case "btCreateContact":
                mEditDialogController.setAction(true);
                mEditDialogController.clearAllFields();
                showEditDialog();
                break;
            case "btEditContact":
                if (contactMainTable.getSelectionModel().getSelectedItem() == null) {
                    return;
                }
                mEditDialogController.setAction(false);
                mEditDialogController.setContactForEdit((Contact) contactMainTable.getSelectionModel().getSelectedItem());
                showEditDialog();
                break;
            case "btDeleteContact":
                mCollectionContactList.deleteContact((Contact) contactMainTable.getSelectionModel().getSelectedItem());
                break;
            case "btnSearch":
                actionSearch();
                break;
        }
    }

    /**
     * Call modal form
     */
    private void showEditDialog() {
        if (mEditDialogStage == null) {
            mEditDialogStage = new Stage();
            mEditDialogStage.setTitle("Contact");
            mEditDialogStage.setMinHeight(400);
            mEditDialogStage.setMinWidth(300);
            mEditDialogStage.setResizable(false);
            mEditDialogStage.setScene(new Scene(mParentEdit));
            mEditDialogStage.initModality(Modality.WINDOW_MODAL);
            mEditDialogStage.initOwner(mainStage);
            mEditDialogController.setMain(mMain);
            mEditDialogController.setCollectionContactList(mCollectionContactList);
        }
        mEditDialogStage.showAndWait();
    }

    /**
     * Execute search from user's request
     */
    public void actionSearch() {
        onceCopyCollection();
        mCollectionContactList.getContactsList().clear();
        for (Contact contact : mTempContactList) {
            String inputtedText = txtSearch.getText().trim().toLowerCase();
            if (contact.getName().toLowerCase().contains(inputtedText) ||
                    contact.getSurname().toLowerCase().contains(inputtedText) ||
                    contact.getPhone().toLowerCase().contains(inputtedText) ||
                    contact.getAddress().toLowerCase().contains(inputtedText) ||
                    contact.getEmail().toLowerCase().contains(inputtedText) ||
                    contact.getBirthday().toString().toLowerCase().contains(inputtedText)) {

                mCollectionContactList.getContactsList().add(contact);
            }
        }
    }

    /**
     * Need for search
     * Copy collection of contacts one time
     */
    private void onceCopyCollection() {
        if (mCountOfContacts > mOldValueCountOfContacts) {
            firstClick = true;
        }

        if (firstClick) {
            mTempContactList = new ArrayList<>(mCollectionContactList.getContactsList());
            mOldValueCountOfContacts = mCountOfContacts;
        }
        firstClick = false;
    }

    /**
     * Call Create new doc
     * @param pActionEvent
     */
    public void actionNewClick(ActionEvent pActionEvent) {
        mMain.getSaveUtils().handleNew();
    }

    /**
     * Call Open dialog
     * @param pActionEvent
     */
    public void actionOpenClick(ActionEvent pActionEvent) {
        mMain.getSaveUtils().handleOpen();
    }

    /**
     * Call save method
     * @param pActionEvent
     */
    public void actionSaveClick(ActionEvent pActionEvent) {
        mMain.getSaveUtils().handleSave();
    }

    /**
     * Call dialog for saving
     * @param pActionEvent
     */
    public void actionSaveAsClick(ActionEvent pActionEvent) {
        mMain.getSaveUtils().handleSaveAs();
    }

    /**
     *Call when user click close program
     * @param pActionEvent
     */
    public void actionCloseClick(ActionEvent pActionEvent) {
        mMain.getSaveUtils().handleExit();
    }

    /**
     * Call when user click About
     * @param pActionEvent
     */
    public void actionAboutClick(ActionEvent pActionEvent) {
        mMain.getSaveUtils().handleAbout();
    }
}
