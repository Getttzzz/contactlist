package contactlist.start;

import contactlist.controllers.MainController;
import contactlist.utils.SaveUtils;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.File;
import java.util.prefs.Preferences;

public class Main extends Application {

    private Stage mPrimaryStage;
    private CollectionContactList mCollectionContactList;
    private SaveUtils mSaveUtils;

    /**
     * Initting primary stage and main controller
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        this.mPrimaryStage = primaryStage;

        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setLocation(getClass().getResource("../xml/main.fxml"));
        Parent fxmlMain = fxmlLoader.load();
        MainController mainController = fxmlLoader.getController();
        mainController.setMainStage(mPrimaryStage);
        mainController.setMain(this);

        mPrimaryStage.setTitle("Contact List");
        mPrimaryStage.setMinHeight(455);
        mPrimaryStage.setMinWidth(700);
        mPrimaryStage.setMaxHeight(460);
        mPrimaryStage.setMaxWidth(710);
        mPrimaryStage.getIcons().add(new Image(getClass().getResource("/contactlist/drawable/ic_contact_phone_black_48dp.png").toString()));
        mPrimaryStage.setScene(new Scene(fxmlMain, 700, 400));
        mPrimaryStage.show();

        mCollectionContactList = mainController.getCollectionContactList();
        mSaveUtils = new SaveUtils(mCollectionContactList, mPrimaryStage);
        mSaveUtils.getLastContactList();
    }

    /**
     * Getter
     * @return
     */
    public SaveUtils getSaveUtils() {
        return mSaveUtils;
    }

    /**
     * Entry point of program
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }
}
